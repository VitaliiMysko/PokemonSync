﻿using Newtonsoft.Json;

namespace PokemonSync.Infrastructure.Dto.Pokemons
{
    public class AbilitiesDto
    {
        public AbilityDto Ability { get; set; }
        [JsonProperty("Is_Hidden")]
        public bool IsHidden { get; set; }
        public int Slot { get; set; }
    }

}
