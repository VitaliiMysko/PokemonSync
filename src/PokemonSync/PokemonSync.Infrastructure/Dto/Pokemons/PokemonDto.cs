﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace PokemonSync.Infrastructure.Dto.Pokemons
{
    public class PokemonDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [JsonProperty("Is_Default")]
        public bool IsDefault { get; set; }
        public int Height { get; set; }
        public int Weight { get; set; }
        public IList<AbilitiesDto> Abilities { get; set; }
        public IList<StatsDto> Stats { get; set; }
        [JsonProperty("Location_Area_Encounters")]
        public string LocationAreaEncounters { get; set; }
    }

}
