﻿namespace PokemonSync.Infrastructure.Dto.Pokemons
{
    public class AbilityDto
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }

}
