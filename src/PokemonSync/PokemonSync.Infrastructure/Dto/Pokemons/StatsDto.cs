﻿using Newtonsoft.Json;

namespace PokemonSync.Infrastructure.Dto.Pokemons
{
    public class StatsDto
    {
        [JsonProperty("Base_Stat")]
        public int BaseStat { get; set; }
        public int Effort { get; set; }
        public StatDto Stat { get; set; }
    }

}
