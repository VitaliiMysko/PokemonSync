﻿using System;
using CsvHelper.Configuration.Attributes;

namespace PokemonSync.Infrastructure.Dto
{
    public class HistoryFightsOfPokemonsDto
    {
        [Name("id")]
        public int Id { get; set; }
        [Name("fighter_name_1")]
        public string FighterName1 { get; set; }
        [Name("fighter_name_2")]
        public string FighterName2 { get; set; }
        //Attack from fighter1
        [Name("attack")]
        public int Attack { get; set; }
        //Defense from fighter2
        [Name("defense")]
        public int Defense { get; set; }
        //Damage that fighter2 got from fighter1
        [Name("damage")]
        public int Damage { get; set; }
        //Hp of fighter2
        [Name("hp")]
        public int Hp { get; set; }
        //fighter2 is dead
        [Name("is_dead")]
        public bool IsADead { get; set; }
        [Name("winner_name")]
        public string WinnerName { get; set; }
        [Name("fight_time")]
        public DateTime FightTime { get; set; }
    }
}
