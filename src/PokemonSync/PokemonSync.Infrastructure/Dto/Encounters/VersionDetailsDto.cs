﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace PokemonSync.Infrastructure.Dto.Encounters
{
    public class VersionDetailsDto
    {
        [JsonProperty("Encounter_Details")]
        public IList<EncounterDetailsDto> EncounterDetails { get; set; }
        [JsonProperty("Max_Chance")]
        public int MaxChance { get; set; }
        public VersionDto Version { get; set; }
    }

}
