﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace PokemonSync.Infrastructure.Dto.Encounters
{
    public class EncounterDetailsDto
    { 
        public int Chance { get; set; }
        [JsonProperty("Condition_Values")]
        public IList<ConditionValuesDto> ConditionValues { get; set; }
        [JsonProperty("Max_Level")]
        public int MaxLevel { get; set; }
        public MethodDto Method { get; set; }
        [JsonProperty("Min_Level")]
        public int MinLevel { get; set; }
    }

}
