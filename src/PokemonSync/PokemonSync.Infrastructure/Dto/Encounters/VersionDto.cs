﻿namespace PokemonSync.Infrastructure.Dto.Encounters
{
    public class VersionDto
    { 
        public string Name { get; set; }
        public string Url { get; set; }
    }

}
