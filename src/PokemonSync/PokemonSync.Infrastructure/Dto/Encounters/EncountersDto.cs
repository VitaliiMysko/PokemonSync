﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace PokemonSync.Infrastructure.Dto.Encounters
{
    public class EncountersDto
    {
        [JsonProperty("Location_Area")]
        public LocationAreaDto LocationArea { get; set; }
        [JsonProperty("Version_Details")]
        public IList<VersionDetailsDto> VersionDetails { get; set; }
    }

}
