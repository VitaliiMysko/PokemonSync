﻿namespace PokemonSync.Infrastructure.Dto.Encounters
{
    public class MethodDto
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }

}
