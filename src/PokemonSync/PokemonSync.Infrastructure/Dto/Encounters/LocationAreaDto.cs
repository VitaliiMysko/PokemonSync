﻿namespace PokemonSync.Infrastructure.Dto.Encounters
{
    public class LocationAreaDto
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }

}
