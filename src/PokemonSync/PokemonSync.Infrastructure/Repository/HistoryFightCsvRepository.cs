﻿using CsvHelper;
using PokemonSync.Domain.Interfaces;
using PokemonSync.Infrastructure.Dto;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using CsvHelper.Configuration;
using PokemonSync.Domain.Entities;
using AutoMapper;

namespace PokemonSync.Infrastructure.Repository
{
    public class HistoryFightCsvRepository : IRepository<HistoryFightsOfPokemons>
    {
        private readonly IMapper _mapper;
        private IList<HistoryFightsOfPokemons> _records = new List<HistoryFightsOfPokemons>();

        public string ConnectionStrings;

        public HistoryFightCsvRepository(IMapper mapper)
        {
            _mapper = mapper;
        }

        private void UpdateRecords()
        {
            _records = ReadCsvFile();
        }

        public void Create(HistoryFightsOfPokemons item)
        {
            UpdateRecords();
            
            var recordsOrder = _records.OrderBy(u => u.Id);

            var lastRecord = recordsOrder.LastOrDefault();

            if (lastRecord != null)
            {
                item.Id = lastRecord.Id + 1;
            }

            _records.Add(item);

            Save();
        }

        public void Delete(int id)
        {
            UpdateRecords();
            
            var itemToRemove = GetRecordById(id);

            if (itemToRemove != null)
            {
                _records.Remove(itemToRemove);
            }

            Save();
        }

        public HistoryFightsOfPokemons GetRecordById(int id)
        {
            UpdateRecords();

            var record = _records.Where(p => p.Id == id);
            
            foreach (var item in record)
            {
                return item;
            }

            return _records.FirstOrDefault();
        }

        public IList<HistoryFightsOfPokemons> GetRecordList()
        {
            UpdateRecords();
            
            return _records;
        }

        public void Update(HistoryFightsOfPokemons item)
        {
            UpdateRecords();

            var record = GetRecordById(item.Id);

            if (record != null)
            {
                var index = _records.IndexOf(record);

                _records[index] = item;
                return;
            }

            Save();
        }

        private void Save()
        {
            WriteCsvFile();
        }

        private List<HistoryFightsOfPokemons> ReadCsvFile()
        {
            if (!File.Exists(ConnectionStrings))
            {
                WriteCsvFile();
            }

            CsvConfiguration config = GetCsvConfiguration();

            using (var reader = new StreamReader(ConnectionStrings))
            using (var csv = new CsvReader(reader, config))
            {
                var recordsDto = csv.GetRecords<HistoryFightsOfPokemonsDto>();
                return _mapper.Map<IList<HistoryFightsOfPokemons>>(recordsDto).ToList();
            }
        }

        private static CsvConfiguration GetCsvConfiguration()
        {
            return new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                Delimiter = ";",
            };
        }

        private void WriteCsvFile()
        {
            var recordsDto = _mapper.Map<IList<HistoryFightsOfPokemonsDto>>(_records);

            CsvConfiguration config = GetCsvConfiguration();

            using (var writer = new StreamWriter(ConnectionStrings))
            using (var csv = new CsvWriter(writer, config))
            {
                csv.WriteRecords(recordsDto);
            }
        }

    }
}
