﻿using AutoMapper;
using PokemonSync.Domain.Entities.Encounters;
using PokemonSync.Domain.Entities.Pokemons;
using PokemonSync.Domain.Interfaces;
using PokemonSync.Domain.Repository;
using PokemonSync.Infrastructure.Dto.Encounters;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace PokemonSync.Infrastructure.Repository
{
    public class EncountersRepository : IRepositoryEncounters
    {
        private readonly HttpClient _httpClient;
        private readonly ISerialize _serialize;
        private readonly IMapper _mapper;

        public EncountersRepository(IHttpClientFactory clientFactory,
            ISerialize serialize,
            IMapper mapper)
        {
            _httpClient = clientFactory.CreateClient("pokeapi");
            _serialize = serialize;
            _mapper = mapper;
        }

        public async Task<IList<Encounters>> Get(string namePokemon)
        {
            string request = "api/v2/pokemon/" + namePokemon + "/encounters";

            var response = await _httpClient.GetAsync(request);
            return await GetData(response);
        }

        public async Task<IList<Encounters>> Get(int idPokemon)
        {
            return await Get(idPokemon.ToString());
        }

        public async Task<IList<Encounters>> Get(Pokemon pokemon)
        {
            var response = await _httpClient.GetAsync(pokemon.LocationAreaEncounters);
            return await GetData(response);
        }

        private async Task<IList<Encounters>> GetData(HttpResponseMessage response)
        {
            response.EnsureSuccessStatusCode();     // throws if not 200-299
            var jsonResult = await response.Content.ReadAsStringAsync();

            var dataDto = _serialize.Deserialize<IList<EncountersDto>>(jsonResult);
            return _mapper.Map<IList<Encounters>>(dataDto);
        }

        public async Task<Encounters> GetRandom(Pokemon pokemon)
        {
            IList<Encounters> encounters = await Get(pokemon);
            int count = encounters.Count;

            if (count == 0)
            {
                return new Encounters();
            }

            Random rnd = new Random();
            int index = rnd.Next(count - 1);
            return encounters[index];
        }
    }
}
