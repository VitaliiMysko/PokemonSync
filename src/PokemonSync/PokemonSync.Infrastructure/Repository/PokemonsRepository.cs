﻿using AutoMapper;
using PokemonSync.Domain.Entities.Pokemons;
using PokemonSync.Domain.Interfaces;
using PokemonSync.Domain.Repository;
using PokemonSync.Infrastructure.Dto.Pokemons;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace PokemonSync.Infrastructure.Repository
{
    public class PokemonsRepository : IRepositoryPokemon
    {
        private readonly HttpClient _httpClient;
        private readonly ISerialize _serialize;
        private readonly IMapper _mapper;
        static readonly int MaxId = 897;

        public PokemonsRepository(IHttpClientFactory clientFactory,
            ISerialize serialize,
            IMapper mapper)
        {

            _httpClient = clientFactory.CreateClient("pokeapi");
            _serialize = serialize;
            _mapper = mapper;
        }

        public async Task<Pokemon> Get(string name)
        {
            string request = "api/v2/pokemon/" + name;
            var response = await _httpClient.GetAsync(request);
            response.EnsureSuccessStatusCode();     // throws if not 200-299
            var jsonResult = await response.Content.ReadAsStringAsync();

            var dataDto = _serialize.Deserialize<PokemonDto>(jsonResult);
            return _mapper.Map<Pokemon>(dataDto);
        }

        public async Task<Pokemon> Get(int id)
        {
            return await Get(id.ToString());
        }

        public async Task<Pokemon> GetRandom()
        {
            Random rnd = new Random();
            int idPokemon = rnd.Next(MaxId) + 1;
            return await Get(idPokemon.ToString());
        }
    }

}
