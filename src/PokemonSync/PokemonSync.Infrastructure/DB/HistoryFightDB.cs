﻿using AutoMapper;
using Dapper;
using PokemonSync.Domain.Entities;
using PokemonSync.Domain.Interfaces;
using PokemonSync.Infrastructure.Dto;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;

namespace PokemonSync.Infrastructure.Db
{
    public class HistoryFightDB : IRepository<HistoryFightsOfPokemons>
    {
        private readonly IMapper _mapper;
        public string ConnectionStrings;

        public HistoryFightDB(
            IMapper mapper)
        {
            _mapper = mapper;
        }

        private IDbConnection GetConnection()
        {
            return new SQLiteConnection(ConnectionStrings);
        }

        public void Create(HistoryFightsOfPokemons item)
        {
            using (IDbConnection con = GetConnection())
            {
                string sql =
                    @"INSERT INTO HistoryFights (fighter_name_1, fighter_name_2, attack, defense, damage, hp, is_a_dead, winner_name, fight_time) 
                    VALUES (@FighterName1, @FighterName2, @Attack, @Defense, @Damage, @Hp, @IsADead, @WinnerName, @FightTime)";
                con.Execute(sql, item);
            }
        }

        public void Delete(int id)
        {
            using (IDbConnection con = GetConnection())
            {
                var parameters = new { Id = id };
                string sql = "DELETE FROM HistoryFights WHERE id=@Id";
                con.Execute(sql, parameters);
            }
        }

        public HistoryFightsOfPokemons GetRecordById(int id)
        {
            using (IDbConnection con = GetConnection())
            {
                var parameters = new { Id = id };
                string sql = "SELECT * FROM HistoryFights WHERE id=@Id";
                var recordsDto = con.Query<HistoryFightsOfPokemonsDto>(sql, parameters);
                return _mapper.Map<IList<HistoryFightsOfPokemons>>(recordsDto).FirstOrDefault();
            }
        }

        public IList<HistoryFightsOfPokemons> GetRecordList()
        {
            using (IDbConnection con = GetConnection())
            {
                string sql = "SELECT * FROM HistoryFights";
                var recordsDto = con.Query<HistoryFightsOfPokemonsDto>(sql);
                return _mapper.Map<IList<HistoryFightsOfPokemons>>(recordsDto).ToList();
            }
        }

        public void Update(HistoryFightsOfPokemons item)
        {
            using (IDbConnection con = GetConnection())
            {
                string sql =
                    @"UPDATE HistoryFights 
                    SET fighter_name_1=@FighterName1, fighter_name_2=@FighterName2, attack=@Attack, defense=@Defense, damage=@Damage, hp=@Hp, is_a_dead=@IsADead, winner_name=@WinnerName, fight_time=@FightTime 
                    WHERE id=@Id";
                con.Execute(sql, item);
            }
        }

    }
}
