﻿using PokemonSync.Domain.Interfaces;
using System.Text.Json;

namespace PokemonSync.Infrastructure.Serialize
{
    public class MsSerialize : ISerialize
    {
        public T Deserialize<T>(string json)
        {
            return JsonSerializer.Deserialize<T>(json, GetJsonOptions());
        }

        public string Serialize(object value)
        {
            return JsonSerializer.Serialize(value);
        }

        private JsonSerializerOptions GetJsonOptions()
        {
            return new JsonSerializerOptions(JsonSerializerDefaults.Web);
        }

    }
}
