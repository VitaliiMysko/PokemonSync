﻿using Newtonsoft.Json;
using PokemonSync.Domain.Interfaces;

namespace PokemonSync.Infrastructure.Serialize
{
    public class NetSerialize : ISerialize
    {
        public T Deserialize<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        public string Serialize(object value)
        {
            return JsonConvert.SerializeObject(value, Formatting.Indented, GetJsonOptions());
        }

        private JsonSerializerSettings GetJsonOptions()
        {
            return new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
            };
        }

    }
}
