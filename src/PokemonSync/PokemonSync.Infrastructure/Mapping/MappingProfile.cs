﻿using AutoMapper;
using PokemonSync.Domain.Entities.Pokemons;
using PokemonSync.Domain.Entities.Encounters;
using PokemonSync.Infrastructure.Dto.Encounters;
using PokemonSync.Infrastructure.Dto.Pokemons;
using PokemonSync.Domain.Entities;
using PokemonSync.Infrastructure.Dto;

namespace PokemonSync.Infrastructure.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<PokemonDto, Pokemon>().ReverseMap();
            CreateMap<AbilitiesDto, Abilities>().ReverseMap();
            CreateMap<AbilityDto, Ability>().ReverseMap();
            CreateMap<StatsDto, Stats>().ReverseMap();
            CreateMap<StatDto, Stat>().ReverseMap();

            CreateMap<EncountersDto, Encounters>().ReverseMap();
            CreateMap<LocationAreaDto, LocationArea>().ReverseMap();
            CreateMap<VersionDetailsDto, VersionDetails>().ReverseMap();
            CreateMap<EncounterDetailsDto, EncounterDetails>().ReverseMap();
            CreateMap<VersionDto, Version>().ReverseMap();
            CreateMap<ConditionValuesDto, ConditionValues>().ReverseMap();
            CreateMap<MethodDto, Method>().ReverseMap();

            CreateMap<HistoryFightsOfPokemonsDto, HistoryFightsOfPokemons>().ReverseMap();
        }
    }
}
