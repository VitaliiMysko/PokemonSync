﻿using System.Collections.Generic;

namespace PokemonSync.Domain.Interfaces
{
    public interface IRepository<T> where T : class
    {
        public IList<T> GetRecordList();
        public T GetRecordById(int id);
        public void Create(T item);
        public void Update(T item);
        public void Delete(int id);
    }
}
