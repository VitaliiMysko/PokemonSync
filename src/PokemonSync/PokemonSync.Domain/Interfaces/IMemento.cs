﻿namespace PokemonSync.Domain.Interfaces
{
    public interface IMemento
    {
        public string Fighter1Name { get; }
        public string Fighter2Name { get; }
        public int Attack { get; }
        public int Defense { get; }
        public int Damage { get; }
        public int HpFighter2 { get; }
        public string AllFightersName { get; }
        string GetDescription();
    }
}
