﻿namespace PokemonSync.Domain.Interfaces
{
    public interface ISerialize
    {
        public T Deserialize<T>(string json);
        public string Serialize(object value);
    }
}
