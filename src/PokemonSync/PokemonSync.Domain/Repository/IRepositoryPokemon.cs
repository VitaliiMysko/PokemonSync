﻿using PokemonSync.Domain.Entities.Pokemons;
using System.Threading.Tasks;

namespace PokemonSync.Domain.Repository
{
    public interface IRepositoryPokemon
    {
        public Task<Pokemon> Get(string name);
        public Task<Pokemon> Get(int id);
        public Task<Pokemon> GetRandom();
    }
}
