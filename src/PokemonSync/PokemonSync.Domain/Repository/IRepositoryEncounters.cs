﻿using PokemonSync.Domain.Entities.Encounters;
using PokemonSync.Domain.Entities.Pokemons;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PokemonSync.Domain.Repository
{
    public interface IRepositoryEncounters
    {
        public Task<IList<Encounters>> Get(string namePokemon);
        public Task<IList<Encounters>> Get(int idPokemon);
        public Task<IList<Encounters>> Get(Pokemon pokemon);
        public Task<Encounters> GetRandom(Pokemon pokemon);
    }
}
