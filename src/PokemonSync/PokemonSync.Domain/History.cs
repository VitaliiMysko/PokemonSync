﻿using PokemonSync.Domain.Entities;
using PokemonSync.Domain.Interfaces;
using System;
using System.Collections.Generic;

namespace PokemonSync.Domain
{
    public class History
    {
        private List<IMemento> _mementos = new List<IMemento>();
        private Fight _fight = null;

        public History(Fight fight)
        {
            _fight = fight;
        }

        public void Backup()
        {
            _mementos.Add(_fight.Save());
        }

        public void Undo()
        {
            int count = _mementos.Count;
            if (_mementos.Count == 0)
            {
                return;
            }

            int idx = count - 1;

            var memento = _mementos[idx];
            _mementos.RemoveAt(idx);

            try
            {
                _fight.Restore(memento);
            }
            catch
            {
                Undo();
            }
        }

        public List<string> ShowHistory()
        {
            List<string> listOfHistory = new List<string>();

            for (int i = 0; i < _mementos.Count; i++)
            {
                var memento = _mementos[i];
                if (i == 0)
                {
                    listOfHistory.Add("FIGHT");
                    listOfHistory.Add(memento.AllFightersName);
                }

                listOfHistory.Add(memento.GetDescription());

                if (i == (_mementos.Count - 1))
                {
                    if (_fight.Winner != null)
                    {
                        string messageAboutWinner = $"WINNER {_fight.Winner.Name}";
                        listOfHistory.Add(messageAboutWinner);
                    }
                }
            }

            return listOfHistory;
        }

        public List<HistoryFightsOfPokemons> HistoryToFile()
        {
            List<HistoryFightsOfPokemons> listOfHistoryToFile = new List<HistoryFightsOfPokemons>();

            for (int i = 0; i < _mementos.Count; i++)
            {
                var memento = _mementos[i];

                string winnerName = "";

                if (i == (_mementos.Count - 1))
                {
                    if (_fight.Winner != null)
                    {
                        winnerName = memento.Fighter1Name;
                    }
                }

                var record = new HistoryFightsOfPokemons
                {
                    FighterName1 = memento.Fighter1Name,
                    FighterName2 = memento.Fighter2Name,
                    Attack = memento.Attack,
                    Defense = memento.Defense,
                    Damage = memento.Damage,
                    Hp = memento.HpFighter2,
                    IsADead = memento.HpFighter2 == 0,
                    WinnerName = winnerName,
                    FightTime = DateTime.Now
                };

                listOfHistoryToFile.Add(record);
            }

            return listOfHistoryToFile;
        }
    }

}
