﻿using System;

namespace PokemonSync.Domain.Entities
{
    public class HistoryFightsOfPokemons
    {
        public int Id { get; set; }
        public string FighterName1 { get; set; }
        public string FighterName2 { get; set; }
        //Attack from fighter1
        public int Attack { get; set; }
        //Defense from fighter2
        public int Defense { get; set; }
        //Damage that fighter2 got from fighter1
        public int Damage { get; set; }
        //Hp of fighter2
        public int Hp { get; set; }
        //fighter2 is dead
        public bool IsADead { get; set; }
        public string WinnerName { get; set; }
        public DateTime FightTime { get; set; }
    }
}
