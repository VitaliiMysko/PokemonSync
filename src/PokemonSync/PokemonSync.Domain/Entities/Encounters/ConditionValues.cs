﻿namespace PokemonSync.Domain.Entities.Encounters
{
    public class ConditionValues
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }

}
