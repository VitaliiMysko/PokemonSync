﻿using System.Collections.Generic;

namespace PokemonSync.Domain.Entities.Encounters
{
    public class EncounterDetails
    { 
        public int Chance { get; set; }
        public IList<ConditionValues> ConditionValues { get; set; }
        public int MaxLevel { get; set; }
        public Method Method { get; set; }
        public int MinLevel { get; set; }
    }

}
