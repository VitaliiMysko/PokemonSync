﻿using System.Collections.Generic;

namespace PokemonSync.Domain.Entities.Encounters
{
    public class VersionDetails
    {
        public IList<EncounterDetails> EncounterDetails { get; set; }
        public int MaxChance { get; set; }
        public Version Version { get; set; }
    }

}
