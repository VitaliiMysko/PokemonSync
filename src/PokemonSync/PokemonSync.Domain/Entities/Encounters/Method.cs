﻿namespace PokemonSync.Domain.Entities.Encounters
{
    public class Method
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }

}
