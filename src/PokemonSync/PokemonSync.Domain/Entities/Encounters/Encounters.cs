﻿using System.Collections.Generic;

namespace PokemonSync.Domain.Entities.Encounters
{
    public class Encounters
    {
        public LocationArea LocationArea { get; set; }
        public IList<VersionDetails> VersionDetails { get; set; }
    }

}
