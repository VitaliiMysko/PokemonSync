﻿namespace PokemonSync.Domain.Entities.Encounters
{
    public class Version
    { 
        public string Name { get; set; }
        public string Url { get; set; }
    }

}
