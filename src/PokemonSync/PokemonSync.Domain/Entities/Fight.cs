﻿using PokemonSync.Domain.Entities.Pokemons;
using PokemonSync.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PokemonSync.Domain.Entities
{
    public class Fight
    {
        private readonly int _countFighters;
        private List<Pokemon> _fighters = new List<Pokemon>();
        private Pokemon _currentFighter1;
        private Pokemon _currentFighter2;
        private int _currentFighterIndex;
        private string _allFightersName;
        private int _countPokemonsLive;

        public Pokemon Winner;

        private History _history;

        public Fight(int countFighters)
        {
            _countFighters = countFighters;
            _history = new History(this);
        }

        public List<HistoryFightsOfPokemons> History
        {
            get => _history.HistoryToFile();
        }

        public string ShowHistory
        {
            get => string.Join("\n", _history.ShowHistory());
        }

        public bool IsReadyToStart
        {
            get => _fighters.Count < _countFighters;
        }

        public void Checkin(Pokemon pokemon)
        {

            if (_fighters.Count > _countFighters)
            {
                throw new Exception($"Pokemon {pokemon.Name} can not take part in fight. This fight is close");
            }

            _fighters.Add(pokemon);

        }

        public void Start()
        {
            if (IsReadyToStart == true)
            {
                throw new Exception($"This fight has too little pokemon ({_fighters} / {_countFighters}, fight is impossible");
            }

            GetAllFightersName();

            _countPokemonsLive = _countFighters;

            Arena();
        }

        private void GetAllFightersName()
        {
            var fightersName = _fighters.Select(p => p.Name);
            _allFightersName = string.Join(", ", fightersName);
        }

        public void Arena()
        {

            for (int i = 0; i < _countFighters; i++)
            {
                var fighter1 = _fighters[i];

                if (fighter1.IsADead())
                {
                    continue;
                }

                int j = i == (_countFighters - 1) ? 0 : i + 1;

                for (; j <= _countFighters; j++)
                {
                    if (j == _countFighters)
                    {
                        j = 0;
                        continue;
                    }

                    var fighter2 = _fighters[j];

                    if (fighter2.IsADead())
                    {
                        continue;
                    }

                    var attackStats = fighter1.AttackStat;
                    fighter2.Defend(attackStats);

                    _currentFighter1 = fighter1;
                    _currentFighter2 = fighter2;
                    _currentFighterIndex = j;

                    if (fighter2.IsADead())
                    {
                        _countPokemonsLive--;
                    }

                    if (_countPokemonsLive == 1)
                    {
                        Winner = fighter1;
                    }

                    _history.Backup();

                    break;
                }

                if (Winner != null)
                {
                    break;
                }

            }

            if (Winner == null)
            {
                Arena();
            }

        }

        public IMemento Save()
        {
            return new FightMemento(_currentFighter1, _currentFighter2, _currentFighterIndex, _allFightersName);
        }

        public void Restore(IMemento memento)
        {
            if (!(memento is FightMemento))
            {
                throw new Exception("Unknown memento class " + memento.ToString());
            }

            //I WILL BE DO IT

        }

    }
}
