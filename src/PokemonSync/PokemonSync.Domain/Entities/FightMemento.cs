﻿using PokemonSync.Domain.Entities.Pokemons;
using PokemonSync.Domain.Interfaces;

namespace PokemonSync.Domain.Entities
{
    public class FightMemento : IMemento
    {
        private int _fighterIndex;
        private string _fighter1Name;
        private string _fighter2Name;
        private int _attack;
        private int _defense;
        private int _damage;
        private string _allFightersName;
        private int _hpFighter2;
        private bool _isADead;

        public FightMemento(Pokemon fighter1, 
            Pokemon fighter2, 
            int fighterIndex,
            string allFightersName)
        {

            _fighter1Name = fighter1.Name;
            _fighter2Name = fighter2.Name;
            _fighterIndex = fighterIndex;
            _attack = fighter2.CurrentFightAttackFromAnotherPokemon;
            _defense = fighter2.CurrenFightDefense;
            _damage = fighter2.CurrentFightDamage;
            _allFightersName = allFightersName;
            _hpFighter2 = fighter2.HP;
            _isADead = fighter2.IsADead();
        }

        public string Fighter1Name
        {
            get => _fighter1Name;
        }

        public string Fighter2Name
        {
            get => _fighter2Name;
        }

        public int Attack
        {
            get =>_attack;
        }

        public int Defense
        {
            get => _defense;
        }

        public int Damage
        {
            get => _damage;
        }

        public int HpFighter2
        {
            get => _hpFighter2;
        }

        public string AllFightersName
        {
            get => _allFightersName;
        }

        public string GetDescription()
        {
            string messageAboutFighters = $"({_fighter1Name} vs {_fighter2Name})";
            string messageAboutFight = $"attack {_attack} --> defense {_defense}\t damege {_damage}\t hp {_hpFighter2} {(_isADead ? "\tDEAD" : "")}";

            return $"{messageAboutFighters} {messageAboutFight}";
        }
    }
}
