﻿using System;

namespace PokemonSync.Domain.Entities.Pokemons
{
    public class Stats
    {
        public int BaseStat { get; set; }
        public int Effort { get; set; }
        public Stat Stat { get; set; }

    }

}
