﻿namespace PokemonSync.Domain.Entities.Pokemons
{
    public class Stat
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }

}
