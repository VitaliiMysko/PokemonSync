﻿using System;
using System.Collections.Generic;

namespace PokemonSync.Domain.Entities.Pokemons
{
    public class Pokemon
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsDefault { get; set; }
        public int Height { get; set; }
        public int Weight { get; set; }
        public IList<Abilities> Abilities { get; set; }
        public IList<Stats> Stats { get; set; }
        public string LocationAreaEncounters { get; set; }
        protected Fight fight { get; set; }

        private int _currentFightAttackFromAnotherPokemon;
        private int _currenFightDefense;
        private int _currentFightDamage;

        enum PokemonStats
        {
            Hp,
            Attack,
            Defense,
            SpecialAttack,
            SpecialDefense,
            Speed
        }

        public int HP
        {
            get => Stats[(int)PokemonStats.Hp].BaseStat;
            set => Stats[(int)PokemonStats.Hp].BaseStat = value;
        }

        public int Attack
        {
            get => Stats[(int)PokemonStats.Attack].BaseStat;
            set => Stats[(int)PokemonStats.Attack].BaseStat = value;
        }

        public int Defense
        {
            get => Stats[(int)PokemonStats.Defense].BaseStat;
            set => Stats[(int)PokemonStats.Defense].BaseStat = value;
        }

        public int Speed
        {
            get => Stats[(int)PokemonStats.Speed].BaseStat;
            set => Stats[(int)PokemonStats.Speed].BaseStat = value;
        }

        public int CurrentFightAttackFromAnotherPokemon
        {
            get => _currentFightAttackFromAnotherPokemon;
        }

        public int CurrenFightDefense
        {
            get => _currenFightDefense;
        }

        public int CurrentFightDamage
        {
            get => _currentFightDamage;
        }

        public void Defend(Stats attackStats)
        {
            int attack = GetRandomValueOfStats(attackStats);
            int defense = GetRandomValueOfStats(DefenseStat);

            int damage = CommputDamage(attack, defense);

            _currentFightAttackFromAnotherPokemon = attack;
            _currenFightDefense = defense;
            _currentFightDamage = damage;

            HP = HP - damage;
        }

        private int CommputDamage(int attack, int defense)
        {
            int damage = attack - defense;

            damage = damage > 0 ? damage : 0;
            damage = damage < HP ? damage : HP;

            return damage;
        }

        public Stats AttackStat
        {
            get => Stats[(int)PokemonStats.Attack];
        }

        public Stats DefenseStat
        {
            get => Stats[(int)PokemonStats.Defense];
        }

        public int GetRandomValueOfStats(Stats stat)
        {
            Random rnd = new Random();
            return rnd.Next(stat.BaseStat - 1);
        }

        public void TakePartIn(Fight fight)
        {
            if (fight.IsReadyToStart == true)
            {
                fight.Checkin(this);
                this.fight = fight;
            }
        }

        public bool IsADead()
        {
            return HP == 0;
        }
    }

}
