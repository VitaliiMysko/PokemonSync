﻿namespace PokemonSync.Domain.Entities.Pokemons
{
    public class Ability
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }

}
