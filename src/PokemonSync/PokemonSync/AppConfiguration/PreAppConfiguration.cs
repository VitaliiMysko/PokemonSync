﻿using PokemonSync.Interfaces;

namespace PokemonSync.AppConfiguration
{
    public class PreAppConfiguration : IPreAppConfiguration
    {
        public void DoConfiguration()
        {
            Dapper.DefaultTypeMap.MatchNamesWithUnderscores = true;
        }

    }
}
