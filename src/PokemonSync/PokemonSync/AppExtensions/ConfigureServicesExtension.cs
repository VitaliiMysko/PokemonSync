﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PokemonSync.Application.Options;
using PokemonSync.Application.Services;
using PokemonSync.Domain.Entities;
using PokemonSync.Domain.Interfaces;
using PokemonSync.Domain.Repository;
using PokemonSync.Infrastructure.Db;
using PokemonSync.Infrastructure.Mapping;
using PokemonSync.Infrastructure.Repository;
using PokemonSync.Infrastructure.Serialize;
using Quartz;
using System;

namespace PokemonSync.AppExtensions
{
    public static class ConfigureServicesExtension
    {
        public static void AddAppConfigurations(this IServiceCollection services, HostBuilderContext hostContext)
        {
            services.Configure<FightOptions>(hostContext.Configuration.GetSection(FightOptions.FightPokemons));
            services.Configure<QuartzOptions>(hostContext.Configuration.GetSection("Quartz"));
        }

        public static void AddAppHttpClients(this IServiceCollection services, IConfiguration config)
        {
            services.AddHttpClient("pokeapi", client =>
            {
                client.BaseAddress = new Uri(config["HttpClient:Pokeapi"]);
            });
        }

        public static void AddAppServices(this IServiceCollection services, IConfiguration config)
        {
            //services.AddTransient<ISerialize, MsSerialize>();
            services.AddTransient<ISerialize, NetSerialize>();
            services.AddTransient<PokemonFightCommand>();
            services.AddTransient<IRepositoryPokemon, PokemonsRepository>();
            services.AddTransient<IRepositoryEncounters, EncountersRepository>();
            
            //services.AddTransient<IRepository<HistoryFightsOfPokemons>>(sp =>
            //{
            //    return new HistoryFightCsvRepository(sp.GetService<IMapper>())
            //    {
            //        ConnectionStrings = config["ConnectionStrings:Csv"]
            //    };
            //});
            
            services.AddTransient<IRepository<HistoryFightsOfPokemons>>(sp =>
            {
                return new HistoryFightDB(sp.GetService<IMapper>())
                {
                    ConnectionStrings = config["ConnectionStrings:SQlite"]
                };
            });
        }

        public static void AddAppQuartzHostedService(this IServiceCollection services)
        {
            services.AddQuartzHostedService(options =>
            {
                options.WaitForJobsToComplete = true;
            });
        }

        public static void AddAppMappers(this IServiceCollection services)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfile());
                //cfg.SourceMemberNamingConvention = new LowerUnderscoreNamingConvention();
                //cfg.DestinationMemberNamingConvention = new PascalCaseNamingConvention();
            });
            var mapper = config.CreateMapper();
            services.AddSingleton(mapper);
        }

    }
}
