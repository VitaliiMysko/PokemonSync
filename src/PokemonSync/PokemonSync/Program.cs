﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PokemonSync.AppConfiguration;
using PokemonSync.AppExtensions;
using PokemonSync.Interfaces;
using Quartz;
using System.Threading.Tasks;

namespace PokemonSync
{
    class Program
    {
        static async Task Main(string[] args)
        {
            using IHost host = CreateHostBuilder(args).Build();

            //IServiceCollection serviceCollectionAnother = new ServiceCollection();
            //serviceCollectionAnother.AddSingleton<IPreAppConfiguration, PreAppConfiguration>();
            //var hostAnother = serviceCollectionAnother.BuildServiceProvider();

            //var preAppConfAnother = hostAnother.GetRequiredService<IPreAppConfiguration>();
            //preAppConfAnother.DoConfiguration();

            var preAppConf = host.Services.GetRequiredService<IPreAppConfiguration>();
            preAppConf.DoConfiguration();

            await host.RunAsync();
        }

        static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostingContext, config) =>
                 {
                     config.AddJsonFile("appsettings.json", true, true);
                     config.AddCommandLine(args);
                 })
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddAppConfigurations(hostContext);
                    services.AddAppServices(hostContext.Configuration);
                    services.AddAppHttpClients(hostContext.Configuration);
                    services.AddQuartz(quartz =>
                    {
                        quartz.AddQuartzConfigurations();
                        quartz.AddJobs(hostContext.Configuration);
                    });
                    services.AddAppQuartzHostedService();
                    services.AddAppMappers();
                    services.AddSingleton<IPreAppConfiguration, PreAppConfiguration>();
                });
        }

    }
}
