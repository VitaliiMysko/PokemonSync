﻿using System.Threading.Tasks;
using PokemonSync.Domain.Entities.Pokemons;
using PokemonSync.Domain.Repository;
using PokemonSync.Domain.Entities;
using Microsoft.Extensions.Logging;
using System;
using Microsoft.Extensions.Options;
using PokemonSync.Application.Options;
using PokemonSync.Domain.Interfaces;

namespace PokemonSync.Application.Services
{
    public class PokemonFightCommand
    {
        private readonly ILogger<PokemonFightCommand> _logger;
        private readonly IRepositoryPokemon _pokemonsRepository;
        private readonly IOptionsMonitor<FightOptions> _fightOptions;
        private readonly IRepository<HistoryFightsOfPokemons> _toRepository;

        public PokemonFightCommand(
            ILogger<PokemonFightCommand> logger,
            IRepositoryPokemon pokemonsRepository,
            IOptionsMonitor<FightOptions> fightOptions,
            IRepository<HistoryFightsOfPokemons> repository)
        {
            _logger = logger;
            _pokemonsRepository = pokemonsRepository;
            _fightOptions = fightOptions;
            _toRepository = repository;
        }

        public async Task Execute()
        {
            int countFighters = _fightOptions.CurrentValue.CountFighters;

            Fight fight = new Fight(countFighters);

            for (int i = 0; i < countFighters; i++)
            {
                Pokemon pokemon = await _pokemonsRepository.GetRandom();

                try
                {
                    pokemon.TakePartIn(fight);
                }
                catch (Exception ex)
                {
                    _logger.LogWarning(ex.Message);
                }
            }

            fight.Start();

            foreach (var record in fight.History)
            {
                _toRepository.Create(record);
            }

            _logger.LogInformation(fight.ShowHistory);

        }

    }
}
