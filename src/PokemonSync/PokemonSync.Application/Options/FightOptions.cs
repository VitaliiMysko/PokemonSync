﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PokemonSync.Application.Options
{
    public class FightOptions
    {
        public const string FightPokemons = "FightPokemons";
        public int CountFighters { get; set; }
    }
}
