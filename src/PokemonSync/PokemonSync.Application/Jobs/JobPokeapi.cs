﻿using Microsoft.Extensions.Logging;
using PokemonSync.Application.Services;
using Quartz;
using System.Net.Http;
using System.Threading.Tasks;

namespace PokemonSync.Application.Jobs
{
    public class JobPokeapi : IJob
    {
        private readonly ILogger<JobPokeapi> _logger;
        private readonly PokemonFightCommand _pokemonService;

        public JobPokeapi(ILogger<JobPokeapi> logger,
            PokemonFightCommand pokemonService
            )
        {
            _logger = logger;
            _pokemonService = pokemonService;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                await _pokemonService.Execute();
            }
            catch (HttpRequestException ex)
            {
                _logger.LogCritical($"{nameof(HttpRequestException)}: {ex.Message}");
            }
        }

    }
}
